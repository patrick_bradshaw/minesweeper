import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Cell from "./Cell";

class CellData {
  constructor(value, pressed, flagged) {
    this.value = value;
    this.pressed = pressed;
    this.flagged = flagged;
  }
}

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardData: []
    };
    this.createBoard();
  }

  createBoard() {
    let size = this.props.size || 10;
    var board = [];
    var i = 0;
    for (i = 0; i < size; i++) {
      var board2 = [];
      var j = 0;
      for (j = 0; j < size; j++) {
        board2.push(new CellData(0, false, false));
      }
      board.push(board2);
    }
    this.state.boardData = board;
  }

  renderBoard() {
    let board = this;
    let size = this.props.size || 10;
    let rowStyle = {
      flex: 1,
      alignSelf: 'stretch',
      flexDirection: 'row'
    };
    var output = [];
    var i = 0;
    for (i = 0; i < size; i++) {
      var output2 = [];
      var j = 0;
      for (j = 0; j < size; j++) {
        let key = i.toString() + "," + j.toString();
        output2.push(<Cell
          key={key}
          location={key}
          value={this.state.boardData[i][j].value}
          board={board}
          pressed={this.state.boardData[i][j].pressed}
          flagged={this.state.boardData[i][j].flagged}
        />);
      }
      let key = i.toString();
      output.push(<View key={key} style={rowStyle}>{output2}</View>);
    }
    return output;
  }

  render() {
    let columnStyle = {
      flex: this.props.flex,
      alignSelf: 'stretch',
      flexDirection: 'column'
    };
    return (
      <View style={columnStyle}>
        { this.renderBoard() }
      </View>
    );
  }

  // Handle touches
  cellTapped(key, value) {
    let i = parseInt(key.split(',')[0], 10);
    let j = parseInt(key.split(',')[1], 10);
    let newBoard = this.state.boardData.slice();
    let newCell = new CellData(newBoard[i][j].value, true, newBoard[i][j].flagged);
    newBoard[i][j] = newCell;
    this.setState({boardData: newBoard});
  }
}

module.exports = Board;
