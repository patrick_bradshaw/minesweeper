import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Board from "./Board"

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      score: 0
    };
  }

  render() {
    let player = this.props.playerNum || 0;
    let gameStyle = {
      marginTop: 20,
      marginBottom: 10,
      marginLeft: 10,
      marginRight: 10,
      flex: 1,
      flexDirection: 'column',
      alignSelf: 'stretch',
      alignItems: 'center'
    };

    return (
      <View style={gameStyle}>
        <Board flex={0.5} size={10}></Board>
      </View>
    );
  }
}

module.exports = Game;
