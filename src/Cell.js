import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';

class Cell extends Component {
  constructor(props) {
    super(props);
    if (this.props.pressed) {
      alert("Pressed");
    }
    this.state = {
      flagged: this.props.flagged,
      pressed: this.props.pressed
    };
    this.props.board = props.board;
    this.props.location = props.location;
  }

  _cellPressed = () => {
    this.props.board.cellTapped(this.props.location, this.props.value);
  }

  render() {
    let value = this.props.value || 0;
    let flagged = this.state.flagged || false;
    let pressed = this.state.pressed || false;
    let display = pressed ? value.toString() : (flagged ? "X" : "");
    let cellStyle = {
      flex: 1,
      margin: 0,
      borderWidth: 2,
      borderColor: '#000000',
      justifyContent: 'center',
      alignItems: 'center'
    };
    let innerStyle = {
      flex: 1,
      margin: 0,
      justifyContent: 'center',
      alignItems: 'center'
    };
    if (!pressed) {
      return (
        <TouchableHighlight style={cellStyle} onPress={this._cellPressed}>
          <View style={innerStyle}>
            <Text>{display}</Text>
          </View>
        </TouchableHighlight>
      );
    } else {
      return (
        <View style={cellStyle}>
          <Text>{display}</Text>
        </View>
      );
    }
  }
}

module.exports = Cell;
