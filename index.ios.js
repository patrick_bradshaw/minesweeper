/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Game from "./src/Game"

export default class Minesweeper extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Game playerNum="1"></Game>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flexDirection: 'row',
    alignSelf:'stretch'
  },
});

AppRegistry.registerComponent('Minesweeper', () => Minesweeper);
